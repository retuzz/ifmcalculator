//
//  IFMTbbm.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/22/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMTbbm: MTLModel, MTLJSONSerializing {
	var tbbmID : NSNumber!
	var name : String!
	
	static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
		return [
			"tbbmID":"id",
			"name" : "name"
		]
	}
}
