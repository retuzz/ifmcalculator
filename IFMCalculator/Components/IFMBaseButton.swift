//
//  IFMBaseButton.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMBaseButton: UIButton {

	let colorObject = IFMColorObject.sharedInstance
	let fontObject = IFMFontObject.sharedInstance
	
	override func awakeFromNib() {
		self.layer.cornerRadius = CGFloat(4)
		self.titleLabel!.font = self.fontObject.bodyCopy2
		self.setTitleColor(UIColor.white, for: UIControlState())
		self.clipsToBounds = true
	}

	func tapSignal() -> RACSignal {
		return self.rac_signal(for: UIControlEvents.touchUpInside)
	}
}
