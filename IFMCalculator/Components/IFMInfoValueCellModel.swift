//
//  IFMInfoValueCellModel.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMInfoValueCellModel: NSObject {
	var title : String!
	var value : String?
	
	var isBold : Bool = false
}
