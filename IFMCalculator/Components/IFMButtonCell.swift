//
//  IFMButtonCell.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMButtonCell: UITableViewCell {
	@IBOutlet weak var button: IFMPrimaryButton!

    override func awakeFromNib() {
        super.awakeFromNib()
		self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
	
	func tappedSignal() -> RACSignal {
		return self.button.tapSignal()
	}
	
	func setTitle(_ title : String) {
	
		self.button.setTitle(title, for: UIControlState())
	}
}
