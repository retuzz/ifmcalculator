//
//  IFMColorObject.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMColorObject: NSObject {

	static let sharedInstance = IFMColorObject()
	
	let tosca = UIColor(red: 56/255.0, green: 203/255.0, blue: 174/255.0, alpha: 1.0)
	let gray = UIColor(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1.0)
	let lineGray = UIColor(red: 241/255.0, green: 241/255.0, blue: 241/255.0, alpha: 1.0)
}
