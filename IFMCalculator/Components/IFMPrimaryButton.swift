	//
//  IFMPrimaryButton.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMPrimaryButton: IFMBaseButton {

	override func awakeFromNib() {
		super.awakeFromNib()
		self.titleLabel?.font = self.fontObject.bodyCopy1Bold
		self.backgroundColor = self.colorObject.tosca
	}

	
}
