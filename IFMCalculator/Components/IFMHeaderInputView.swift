//
//  IFMHeaderInputView.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMHeaderInputView: UIView {
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var separatorView: UIView!
	
	override func awakeFromNib() {
		self.titleLabel.font = IFMFontObject.sharedInstance.bodyCopy3
		self.titleLabel.textColor = IFMColorObject.sharedInstance.tosca
		self.separatorView.backgroundColor = IFMColorObject.sharedInstance.tosca
		self.backgroundColor = IFMColorObject.sharedInstance.gray
	}

}
