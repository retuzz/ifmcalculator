//
//  IFMInputCell.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMInputCell: UITableViewCell {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var valueLabel: UILabel!
	
	var cellModel : IFMInputCellModel?
	
	@IBOutlet weak var separatorView: UIView!
	override func awakeFromNib() {
		super.awakeFromNib()
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
	
	func setupViews() {
		self.titleLabel.font = IFMFontObject.sharedInstance.bodyCopy2Light
		self.valueLabel.font = IFMFontObject.sharedInstance.bodyCopy2Light
		self.separatorView.backgroundColor = IFMColorObject.sharedInstance.tosca
		
		self.selectionStyle = UITableViewCellSelectionStyle.none
	
	}
	
	func bindViewModel(_ viewModel : IFMInputCellModel) {
		self.cellModel = viewModel
		self.titleLabel.text = viewModel.title
		self.valueLabel.text = viewModel.value
	}
	
}
