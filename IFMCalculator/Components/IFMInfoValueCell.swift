//
//  IFMInfoValueCell.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMInfoValueCell: UITableViewCell {

	@IBOutlet weak var separatorView: UIView!
	@IBOutlet weak var valueLabel: UILabel!
	@IBOutlet weak var titleLabel: UILabel!
	
	var cellModel : IFMInfoValueCellModel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setupViews() {
		self.titleLabel.font = IFMFontObject.sharedInstance.bodyCopy2Light
		self.valueLabel.font = IFMFontObject.sharedInstance.bodyCopy2Light
		self.separatorView.backgroundColor = IFMColorObject.sharedInstance.tosca
		
		self.selectionStyle = UITableViewCellSelectionStyle.none
		
	}
	
	func bindViewModel(_ viewModel : IFMInfoValueCellModel) {
		self.cellModel = viewModel
		self.titleLabel.text = viewModel.title
		self.valueLabel.text = viewModel.value
		
		if viewModel.isBold {
			self.titleLabel.font = IFMFontObject.sharedInstance.bodyMedium2
			self.valueLabel.font = IFMFontObject.sharedInstance.bodyMedium2
		}
	}
}
