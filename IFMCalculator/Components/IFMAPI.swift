//
//  IFMAPI.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMAPI: NSObject {

	var networkManager : AFHTTPRequestOperationManager!
	
	override init() {
		super.init()
		self.networkManager = AFHTTPRequestOperationManager()
		self.networkManager.requestSerializer = AFJSONRequestSerializer()
		
		let responseSerializer = AFJSONResponseSerializer()
		responseSerializer.readingOptions = JSONSerialization.ReadingOptions.allowFragments
		self.networkManager.responseSerializer = responseSerializer
		self.networkManager.responseSerializer.acceptableContentTypes = Set(["text/plain","text/html", "application/json"])
	}
	
	func setupToken(_ token : String) {
		self.networkManager.requestSerializer.setValue(token, forHTTPHeaderField: "Authorization")
	}
	
	func signalGET(_ path : String, parameters : [AnyHashable: Any]?) -> RACSignal {
		self.networkManager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
		return self.networkManager.rac_GET(path, parameters: parameters)
	}
	
	func signalPOST(_ path : String, parameters : [AnyHashable: Any]?) -> RACSignal {
		self.networkManager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
		return self.networkManager.rac_POST(path, parameters: parameters)
	}
	
	
}
