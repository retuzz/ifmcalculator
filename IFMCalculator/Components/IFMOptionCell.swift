//
//  IFMOptionCell.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMOptionCell: UITableViewCell {

	@IBOutlet weak var valueTitle: UILabel!
	@IBOutlet weak var checkImageView: UIImageView!
	
	@IBOutlet weak var separatorView: UIView!
	var cellModel : IFMOptionCellModel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        self.valueTitle.font = IFMFontObject.sharedInstance.bodyCopy2Light
		self.separatorView.backgroundColor = IFMColorObject.sharedInstance.tosca
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
	
	func bindViewModel(_ viewModel : IFMOptionCellModel) {
		self.cellModel = viewModel
		self.valueTitle.text = viewModel.value
		self.checkImageView.isHidden = !viewModel.selected
	}
    
}
