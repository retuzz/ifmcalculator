//
//  IFMPeriode.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/22/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMPeriode: MTLModel, MTLJSONSerializing {
	
	var periodeID : NSNumber!
	var name : NSNumber!
	
	static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
		return [
			"periodeID":"id",
			"name" : "name"
		]
	}
}
