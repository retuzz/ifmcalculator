//
//  IFMAPIService.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/22/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMAPIService: IFMAPI {

	let fullURL = "http://ifmptmn.com/calculator"
	
	let tbbmEndpoint = "/tbbm"
	let periodEndpoint = "/period"
	let productEndpoint = "/product"
	let mopsPublishEndpoint = "/mops_and_publish"
	let hppEndpoint = "/hpp"
	
	let loginEndpoint = "/login"

	
	func fetchProducts() -> RACSignal {
		let url = fullURL + productEndpoint
		return self.signalGET(url, parameters: nil)
	}

	func fetchPeriod() -> RACSignal {
		let url = fullURL + periodEndpoint
		return self.signalGET(url, parameters: nil)
	}
	
	func fetchTbbm() -> RACSignal {
		let url = fullURL + tbbmEndpoint
		return self.signalGET(url, parameters: nil)
	}
	
	func fetchHargaPublishMOPS(_ productID: NSNumber, monthID : NSNumber , periodeID : NSNumber) -> RACSignal {

		let url = String(format: "%@%@/%d/%@/%d", arguments: [fullURL, mopsPublishEndpoint, productID.int32Value, monthID.stringValue, periodeID.int32Value])
		
		return self.signalGET(url, parameters: nil)
	}
	
	func fetchHpp(_ productID: NSNumber, monthID : NSNumber , periodeID : NSNumber, tbbmID : NSNumber) -> RACSignal {
		let url = String(format: "%@%@/%d/%@/%d/%d", arguments: [fullURL, hppEndpoint, productID.int32Value, monthID.stringValue, periodeID.int32Value, tbbmID.int32Value])
		
		return self.signalGET(url, parameters: nil)
	}
	
	func login(_ parameters : [AnyHashable: Any]) -> RACSignal {
		let url = fullURL + loginEndpoint
		return self.signalPOST(url, parameters: parameters)
	}
}
