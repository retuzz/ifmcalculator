//
//  IFMProduct.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/22/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMProduct:  MTLModel, MTLJSONSerializing {

	var productID : NSNumber!
	var name : String!
	
	static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
		return [
			"productID":"id",
			"name" : "name"
		]
	}
}
