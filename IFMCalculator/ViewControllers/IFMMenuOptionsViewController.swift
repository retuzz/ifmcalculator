//
//  IFMMenuOptionsViewController.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMMenuOptionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
	
	var tableView : UITableView!
	var viewModel : IFMMenuOptionsViewModel!
	
	let selectedSubject : RACSubject = RACSubject()
	
	init(viewModel : IFMMenuOptionsViewModel) {
		super.init(nibName: "IFMMenuOptionsViewController", bundle: nil)
		self.viewModel = viewModel
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupViews()
		self.setNavigationBackBar()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func setNavigationBackBar() {
		self.navigationItem.hidesBackButton = true
		let menuButtonImg = UIImage(named: "back")
		let leftMenuButton = UIBarButtonItem(image: menuButtonImg?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), style: UIBarButtonItemStyle.plain, target: self, action: #selector(navigateBack))
		leftMenuButton.tintColor = IFMColorObject.sharedInstance.tosca
		
		
		self.navigationItem.leftBarButtonItem = leftMenuButton
	}
	
	func navigateBack() {
		_ = self.navigationController?.popViewController(animated: true)
	}
	
	func setupViews() {
		self.tableView = UITableView()
		self.view.addSubview(self.tableView)
		
		self.tableView.mas_makeConstraints { (make: MASConstraintMaker?) -> Void in
			if let make = make {
				make.leading.mas_equalTo()(0)
				make.trailing.mas_equalTo()(0)
				make.top.mas_equalTo()(0)
				make.bottom.mas_equalTo()(0)
			}
		}
		self.tableView.register(UINib(nibName: "IFMOptionCell", bundle: nil), forCellReuseIdentifier: "optionCell")
		
		self.tableView.dataSource = self
		self.tableView.delegate = self
		self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
		self.tableView.tableFooterView = UIView()

	}
	
	//MARK -
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.viewModel.cellModels.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "optionCell") as! IFMOptionCell
		
		cell.bindViewModel(self.viewModel.cellModels[(indexPath as NSIndexPath).row])
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 40
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.viewModel.selectRow((indexPath as NSIndexPath).row)
		self.tableView.reloadData()
		
		self.selectedSubject.sendNext((indexPath as NSIndexPath).row)
		_ = self.navigationController?.popViewController(animated: true)
	}
	
	
	func selectedSignal() -> RACSignal {
		return self.selectedSubject
	}
	
}
