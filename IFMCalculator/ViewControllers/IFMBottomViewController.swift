//
//  IFMBottomViewController.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

class IFMBottomViewController: UITabBarController, UITabBarControllerDelegate {
	
	init() {
		super.init(nibName: nil, bundle: nil)
		
	}
	
	required init(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func initializeTabbar() {
		self.delegate = self
		self.setupTabBarMenuItems()
		self.selectedViewController = self.viewControllers?.first as? UINavigationController
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		self.tabBar.barTintColor = UIColor.white
		self.tabBar.isTranslucent = false
		self.initializeTabbar()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	fileprivate func setupTabBarMenuItems() -> Void {
		let priceVC = IFMPriceViewController()
		self.addTabBarViewController(priceVC, imageName: "Price_inactive", selectedImageName: "PriceG_active", tag: 0, title: "Price")
		
		let discountVC = IFMDiscountViewController()
		self.addTabBarViewController(discountVC, imageName: "Discount_inactive", selectedImageName: "DiscountG_active", tag: 1, title: "Discount")
		
		let marginVC = IFMMarginViewController()
		self.addTabBarViewController(marginVC, imageName: "margin_inactive", selectedImageName: "marginG_active", tag: 2, title: "Margin")
		
	}
	
	func addTabBarViewController(_ vc: IFMBaseViewController, imageName: String, selectedImageName: String, tag: Int, title: String) {
		let navigationVC: UINavigationController = self.tabBarItem(vc, imageName: imageName, selectedImageName: selectedImageName, tag: tag, title: title)
		self.addToViewControllers(navigationVC)
		
	}
	
	func tabBarItem(_ vc: IFMBaseViewController, imageName: String, selectedImageName: String, tag: Int, title: String) -> UINavigationController {
		vc.tabBarItem.image = UIImage(named: imageName)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
		vc.tabBarItem.selectedImage = UIImage(named: selectedImageName)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
		vc.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		vc.tabBarItem.titlePositionAdjustment = (UIOffsetMake(0, -5))
		vc.tabBarItem.tag = tag
		vc.tabBarItem.title = title
		vc.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.black], for: UIControlState())
		vc.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName : IFMColorObject.sharedInstance.tosca], for: UIControlState.selected)
		let navigationVC = UINavigationController(rootViewController: vc as UIViewController)
		return navigationVC
	}
	
	func addToViewControllers(_ viewController: AnyObject) {
		var viewControllersMutable: NSMutableArray
		if (self.viewControllers != nil) {
			viewControllersMutable = NSMutableArray(array: self.viewControllers!)
		} else {
			viewControllersMutable = NSMutableArray()
		}
		viewControllersMutable.add(viewController)
		self.viewControllers = viewControllersMutable.copy() as? [UIViewController]
	}
	
}
