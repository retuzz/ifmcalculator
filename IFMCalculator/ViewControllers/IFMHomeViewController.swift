//
//  IFMHomeViewController.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMHomeViewController: UIViewController, UITextFieldDelegate {
	@IBOutlet weak var usernameTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var startButton: IFMPrimaryButton!
	let apiService = IFMAPIService()
	let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
	let colorObject = IFMColorObject.sharedInstance
	let fontObject = IFMFontObject.sharedInstance

	@IBOutlet weak var topConstraint: NSLayoutConstraint!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IFMHomeViewController.viewTapped))
		self.view.addGestureRecognizer(tapGesture)
		
		self.usernameTextField.font = self.fontObject.bodyCopy2
		self.passwordTextField.font = self.fontObject.bodyCopy2
		self.passwordTextField.isSecureTextEntry = true
		self.usernameTextField.placeholder = "username"
		self.passwordTextField.placeholder = "password"
		self.usernameTextField.autocorrectionType = UITextAutocorrectionType.no
		self.passwordTextField.autocorrectionType = UITextAutocorrectionType.no
		self.usernameTextField.delegate = self
		self.passwordTextField.delegate = self
		self.usernameTextField.returnKeyType = UIReturnKeyType.next
		self.passwordTextField.returnKeyType = UIReturnKeyType.done
		
		self.startButton.tapSignal().subscribeNext { (_ : Any?) in
			self.login()
		}
		
		self.topConstraint.constant = (self.screenHeight() - 296)/2			
		self.observeKeyboard()
    }
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField.placeholder == "username" {
			self.passwordTextField.becomeFirstResponder()
		}
		else {
			textField.resignFirstResponder()
		}
		return true
	}
	
	func screenHeight() -> CGFloat {
		return UIScreen.main.bounds.size.height;
	}
	
	func screenWidth() -> CGFloat {
		return UIScreen.main.bounds.size.width;
	}
	
	func viewTapped() {
		self.view.endEditing(true)
	}
	
	fileprivate func observeKeyboard() {
		NotificationCenter.default.addObserver(self, selector:#selector(IFMHomeViewController.keyboardWillAppear(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector:#selector(IFMHomeViewController.keyboardWillDisappear(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
	}
	
	func keyboardWillAppear(_ notification: Notification){
		let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
		self.topConstraint.constant = self.screenHeight() - 310 - (keyboardSize?.height)!
	}
	
	func keyboardWillDisappear(_ notification: Notification){
		self.topConstraint.constant = (self.screenHeight() - 296)/2
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func fetchAllData () {
		
		let productSignal = self.apiService.fetchProducts()
		let tbbmSignal = self.apiService.fetchTbbm()
		let periodeSignal = self.apiService.fetchPeriod()

		RACSignal.combineLatest(NSArray(array:[productSignal,tbbmSignal, periodeSignal])).subscribeNext({ (obj : Any!) in
			
			if let tuple = obj as? RACTuple {
				
				self.mapProduct(tuple: tuple.first as! RACTuple)
				self.mapTbbm(tuple: tuple.second as! RACTuple)
				self.mapPeriode(tuple: tuple.third as! RACTuple)

			}
			DispatchQueue.main.async(execute: {
				SVProgressHUD.dismiss()
			})
			self.navigateToMainContent()
		}) { (error : Error?) in
			
		}
	}
	
	func mapProduct(tuple : RACTuple) {
		let productsArrayDict = tuple.second as! [String : AnyObject]

		let productsArray = productsArrayDict["products"] as! [AnyObject]
		do {
			let products = try MTLJSONAdapter.models(of: IFMProduct.self, fromJSONArray: productsArray ) as! [IFMProduct]
			TMCache.shared().setObject(products as NSCoding!, forKey: "ifmproducts")
		} catch _ as NSError {

		}
	}
	
	func mapTbbm(tuple: RACTuple) {
		let tbbmDict = tuple.second as! [String : AnyObject]
		let tbbmArray = tbbmDict["tbbm"] as! [AnyObject]
		do {
			let tbbm = try MTLJSONAdapter.models(of: IFMTbbm.self, fromJSONArray: tbbmArray ) as! [IFMTbbm]
			TMCache.shared().setObject(tbbm as NSCoding!, forKey: "ifmtbbm")

		} catch _ as NSError {
		}
	}
	
	func mapPeriode(tuple : RACTuple) {
		let periodDict = tuple.second as! [String : AnyObject]

		let periodArray = periodDict["periods"] as! [AnyObject]
		do {
			let period = try MTLJSONAdapter.models(of: IFMPeriode.self, fromJSONArray: periodArray ) as! [IFMPeriode]
			TMCache.shared().setObject(period as NSCoding!, forKey: "ifmperiods")
		} catch _ as NSError {
		}
	}
	
	func navigateToMainContent() {
		let vc = IFMBottomViewController()
		self.appDelegate.window?.rootViewController = vc
	}
	
	func login() {
		DispatchQueue.main.async(execute: {
			SVProgressHUD.show(with: SVProgressHUDMaskType.gradient)
		})
		
		if let username = self.usernameTextField.text, let password = self.passwordTextField.text {
		let parameters : [AnyHashable: Any] = ["username" : username, "password" : password]
			apiService.login(parameters).subscribeNext({ (obj : Any!) in
				print(obj)
				

				if let tuple = obj as? RACTuple {
					if let dict = tuple.second as? [AnyHashable: Any] {
						let token = dict["token"] as! String
						let defaults = UserDefaults.standard
						defaults.set(token, forKey: "token")
						
						self.apiService.setupToken(token)
						self.fetchAllData()
					}
				}
				
				}, error: { (error : Error?) in
					if let nserror = error as? NSError {
						if let operation =  nserror.userInfo["AFHTTPRequestOperation"] as? AFHTTPRequestOperation {
							if let response = operation.response {
								if response.statusCode == 404 {
									SVProgressHUD.showError(withStatus: "Invalid username or password", maskType: SVProgressHUDMaskType.gradient)
								}
							}
							else {
								SVProgressHUD.showError(withStatus: nserror.localizedDescription, maskType: SVProgressHUDMaskType.gradient)
							}
						}
					}
			})
			
		}
		else {
			DispatchQueue.main.async(execute: {
				SVProgressHUD.showError(withStatus: "Invalid username or password", maskType: SVProgressHUDMaskType.gradient)
			})
		}
	}
	
}
