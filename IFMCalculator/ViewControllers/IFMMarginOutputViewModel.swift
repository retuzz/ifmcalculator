//
//  IFMMarginOutputViewModel.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/22/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMMarginOutputViewModel: NSObject {
	var discountCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var netValueCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var ppnCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var pbbkbCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var pphCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var billingValueRoundCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	
	var firstSectionBillingCellModels : [IFMInfoValueCellModel]!
	
	var volumeCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var discountTotalCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var netValueTotalCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var ppnTotalCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var pbbkbTotalCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var pphTotalCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var billingValueRoundTotalCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var billingValueTotalCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	
	var secondSectionBillingCellModels : [IFMInfoValueCellModel]!
	
	var publishPriceCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var hppCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var discountPriceCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var priceGivenCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var bphAmountCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var bphPercentCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var proposedMarginCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var marginPercentCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	var marginAmountCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	
	var profitCellModel : IFMInfoValueCellModel =  IFMInfoValueCellModel()
	
	var firstSectionCalculationCellModels  : [IFMInfoValueCellModel]!
	
	override init() {
		super.init()
		self.setupBillingCellModels()
		self.setupPriceCellModels()
		
	}
	
	init(publishedPrice: Double, mops : Double, margin : Double,pbbKb : Double, volume : Double, hpp: Double, basedOnMops : Bool) {
		super.init()
		self.setupBillingCellModels()
		self.setupPriceCellModels()
	
		self.volumeCellModel.value = String(volume)
		self.byprice(publishedPrice, mops: mops, margin: margin, pbbkb: pbbKb, volume: volume, hpp: hpp, basedOnMops: basedOnMops)
		self.calcmargin(publishedPrice, mops: mops, hpp: hpp, fake_margin: margin, volume: volume, pbbkb: pbbKb,  basedOnMops: basedOnMops)
	}
	
	func setupBillingCellModels() {
		self.discountCellModel.title = "Discount (Rp)"
		self.netValueCellModel.title = "Net Value (Rp)"
		self.ppnCellModel.title = "PPN (Rp)"
		self.pbbkbCellModel.title = "PBBKB (Rp)"
		self.pphCellModel.title = "PPh (Rp)"
		self.billingValueRoundCellModel.title = "Billing Value bf Rounding (Rp)"
		self.billingValueRoundCellModel.isBold = true
		
		self.firstSectionBillingCellModels = [discountCellModel, netValueCellModel, ppnCellModel, pbbkbCellModel, pphCellModel, billingValueRoundCellModel]
		
		self.volumeCellModel.title = "Volume (KL)"
		self.discountTotalCellModel.title = "Discount (Rp)"
		self.netValueTotalCellModel.title = "Net Value (Rp)"
		self.ppnTotalCellModel.title = "PPN (Rp)"
		self.pbbkbTotalCellModel.title = "PBBKB (Rp)"
		self.pphTotalCellModel.title = "PPh (Rp)"
		self.billingValueRoundTotalCellModel.title = "Billing Value bf Rounding (Rp)"
		self.billingValueTotalCellModel.title = "Billing Value (Rp)"
		self.billingValueTotalCellModel.isBold = true
		
		self.secondSectionBillingCellModels = [volumeCellModel, discountTotalCellModel, netValueTotalCellModel, ppnTotalCellModel, pbbkbTotalCellModel, pphTotalCellModel, billingValueRoundTotalCellModel, billingValueTotalCellModel]
		
	}
	
	func setupPriceCellModels() {
		publishPriceCellModel.title = "Publish Price (%)"
		hppCellModel.title = "HPP (%)"
		discountPriceCellModel.title = "Discount (%)"
		priceGivenCellModel.title = "Price Given (%)"
		priceGivenCellModel.isBold = true
		bphAmountCellModel.title = "BPH (Rp/KL)"
		bphPercentCellModel.title = "BPH (%)"
		marginAmountCellModel.title = "Margin (Rp/KL)"
		proposedMarginCellModel.title = "Propose Margin (%)"
		proposedMarginCellModel.isBold = true
		marginPercentCellModel.title = "Margin (%)"
		marginPercentCellModel.isBold = true
		
		firstSectionCalculationCellModels = [publishPriceCellModel, hppCellModel, discountPriceCellModel, priceGivenCellModel, bphAmountCellModel, bphPercentCellModel, proposedMarginCellModel , marginPercentCellModel, marginAmountCellModel]
		
		profitCellModel.title = "Volume Total (Rp)"
		profitCellModel.isBold = true
	}
	
	func byprice(_ published_price: Double, mops: Double, margin: Double, pbbkb: Double, volume: Double, hpp : Double, basedOnMops : Bool){
		
		var newMargin = margin
		if basedOnMops == false {
			newMargin = margin * published_price / mops
		}
		
		let discount = (published_price / mops * 100) - (hpp * 100) - newMargin
		let discount_rp = discount / 100 * mops
		self.discountCellModel.value =  self.formatNumber(discount_rp)
		
		let basic_price = published_price - discount_rp
		self.netValueCellModel.value = self.formatNumber(basic_price)
		
		let ppn = 10/100 * basic_price
		self.ppnCellModel.value =  self.formatNumber(ppn)
		
		let pbbkb_rp = pbbkb/100 * basic_price
		self.pbbkbCellModel.value =  self.formatNumber(pbbkb_rp)
		
		let pph = 0.3/100 * basic_price
		self.pphCellModel.value =  self.formatNumber(pph)
		
		let billing_value = basic_price + ppn + pbbkb_rp + pph
		self.billingValueRoundCellModel.value = self.formatNumber(billing_value)
		
		let discount_rp_total = discount_rp * volume
		self.discountTotalCellModel.value  =  self.formatNumber(discount_rp_total)
		
		let basic_price_total = basic_price * volume
		self.netValueTotalCellModel.value  = self.formatNumber(basic_price_total)
		
		let ppn_total = ppn * volume
		self.ppnTotalCellModel.value  =  self.formatNumber(ppn_total)
		
		let pbbkb_rp_total = pbbkb_rp * volume
		self.pbbkbTotalCellModel.value =   self.formatNumber(pbbkb_rp_total)
		
		let pph_total = pph * volume
		self.pphTotalCellModel.value =  self.formatNumber(pph_total)
		
		let billing_value_before_round = billing_value * volume
		self.billingValueRoundTotalCellModel.value = self.formatNumber(billing_value_before_round)
		
		let billing_value_round = (round(billing_value_before_round / 1000)) * 1000
		self.billingValueTotalCellModel.value = self.formatNumber(billing_value_round)
	}
	
	func calcmargin(_ published_price: Double,mops: Double,hpp: Double,fake_margin: Double,volume: Double,pbbkb: Double, basedOnMops : Bool) {
		
		var newFakeMargin = fake_margin
		
		if !basedOnMops {
			newFakeMargin = fake_margin * published_price / mops
		}
		
		let publishPricePercent = published_price / mops * 100
		self.publishPriceCellModel.value = self.formatNumber(publishPricePercent)
		
		let hpp_percent = hpp * 100
		self.hppCellModel.value = self.formatNumber(hpp_percent)
		
		let discount = (published_price / mops * 100) - (hpp * 100) - newFakeMargin
		self.discountPriceCellModel.value = self.formatNumber(discount)
		
		let discount_rp = discount / 100 * mops
		print ("Discount Rp")
		print (discount_rp)
		
		let basic_price = published_price - discount_rp
		let basic_price_percent = basic_price / mops * 100
		self.priceGivenCellModel.value =  self.formatNumber(basic_price_percent)
		
		let published_price_mops = published_price / mops * 100
		let bph = 0.3/100 * basic_price
		self.bphAmountCellModel.value = self.formatNumber(bph)
		
		let bph_mops = bph / mops * 100
		self.bphPercentCellModel.value = self.formatNumber(bph_mops)
		
		let margin = published_price_mops - hpp_percent - discount - bph_mops
		self.marginPercentCellModel.value =  self.formatNumber(margin)
		
		let profit_perkl = margin/100 * mops
		self.marginAmountCellModel.value = self.formatNumber(profit_perkl)
		
		let total_profit = margin/100 * mops * volume
		self.profitCellModel.value = self.formatNumber(total_profit)
		
		self.proposedMarginCellModel.value = self.formatNumber(newFakeMargin)
	}
	
	func formatNumber(_ number : Double) -> String {
		let formatter = NumberFormatter()
		formatter.locale = Locale.current
		formatter.numberStyle = NumberFormatter.Style.decimal
		formatter.maximumFractionDigits = 2
		
		return formatter.string(from: NSNumber(value: number as Double))!
	}
}
