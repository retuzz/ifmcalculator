//
//  IFMMarginViewController.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

class IFMMarginViewController: IFMBaseViewController, UITableViewDataSource, UITableViewDelegate {
	var tableView : UITableView!
	var viewModel : IFMMarginViewModel!
	var calculateCell : IFMButtonCell!
	var marginField: UITextField!
	var volumeField: UITextField!
	
	init() {
		super.init(nibName: nil, bundle: nil)
	}
	
	required init(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		self.title = "Margin"
		self.setDefaultNavigation()
		self.setupViewModel()
		self.setupViews()
		self.setupInitialCells()
	}
	
	func setDefaultNavigation(){
		self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName : IFMFontObject.sharedInstance.bodyCopy1!, NSForegroundColorAttributeName: IFMColorObject.sharedInstance.tosca]
		self.navigationController?.navigationBar.isHidden = false
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.shadowImage = nil
	}
	
	
	func setupViewModel() {
		self.viewModel = IFMMarginViewModel()
		self.viewModel.refreshSignal().subscribeNext { (_ : Any!) in
			self.tableView.reloadData()
		}
	}
	
	func setupViews() {
		self.tableView = UITableView()
		self.view.addSubview(self.tableView)
		
		self.tableView.mas_makeConstraints { (make: MASConstraintMaker?) -> Void in
			if let make = make {
				make.leading.mas_equalTo()(0)
				make.trailing.mas_equalTo()(0)
				make.top.mas_equalTo()(0)
				make.bottom.mas_equalTo()(0)
			}
		}
		self.tableView.register(UINib(nibName: "IFMInputCell", bundle: nil), forCellReuseIdentifier: "inputCell")
		self.tableView.register(UINib(nibName: "IFMInfoValueCell", bundle: nil), forCellReuseIdentifier: "infoValueCell")
		self.tableView.register(UINib(nibName: "IFMButtonCell", bundle: nil), forCellReuseIdentifier: "buttonCell")
		
		self.tableView.dataSource = self
		self.tableView.delegate = self
		self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
		self.tableView.tableFooterView = UIView()
	}
	
	func setupInitialCells() {
		self.calculateCell = self.tableView.dequeueReusableCell(withIdentifier: "buttonCell") as! IFMButtonCell
		self.calculateCell.tappedSignal().subscribeNext { (obj : Any!) in
			DispatchQueue.main.async(execute: {
				SVProgressHUD.show(with: SVProgressHUDMaskType.gradient)
			})
			
			self.viewModel.getHpp().subscribeNext({ (_ : Any!) in
				DispatchQueue.main.async(execute: {
					SVProgressHUD.dismiss()
				})
				self.navigateToOutput()
				}, error: { (error : Error?) in
					DispatchQueue.main.async(execute: {
						if let nserror = error as? NSError {
							
							 if let operation =  nserror.userInfo["AFHTTPRequestOperation"] as? AFHTTPRequestOperation {
								if let response = operation.response {
									if response.statusCode == 404 {
										SVProgressHUD.showError(withStatus: "Data not found", maskType: SVProgressHUDMaskType.gradient)
									}
								}
								else {
									SVProgressHUD.showError(withStatus: nserror.localizedDescription, maskType: SVProgressHUDMaskType.gradient)
								}
							}
						}
						else {
							SVProgressHUD.showError(withStatus: "Incomplete form", maskType: SVProgressHUDMaskType.gradient)
						}
						
					})
			})
			
		}
		self.calculateCell.setTitle("CALCULATE")
	}
	
	//MARK : Table view Data source delegate
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 4
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 {
			return 6
		}
		else if section == 1 {
			return 2
		}
		else if section == 2{
			return 2
		}
		else {
			return 1
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if (indexPath as NSIndexPath).section == 0 {
			return self.inputCell(indexPath)
		}
		else if  (indexPath as NSIndexPath).section == 1 {
			if (indexPath as NSIndexPath).row == 0 {
				return self.marginBasedOnCell()
			}
			return self.marginCell()
		}
		else if (indexPath as NSIndexPath).section == 2 {
			return self.informationCell(indexPath)
		}
		else {
			return self.calculateCell
		}
	}
	
	func inputCell(_ indexPath : IndexPath) -> UITableViewCell {
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "inputCell") as! IFMInputCell
		cell.bindViewModel(self.viewModel.formCellModels[(indexPath as NSIndexPath).row])
		return cell
		
	}
	
	func marginCell() -> UITableViewCell {
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "infoValueCell") as! IFMInfoValueCell
		
		cell.bindViewModel(self.viewModel.marginCellModel)
		return cell
	}
	
	func marginBasedOnCell() -> UITableViewCell {
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "inputCell") as! IFMInputCell
		
		cell.bindViewModel(self.viewModel.marginBasedOnCellModel)
		return cell
	}
	
	func informationCell(_ indexPath : IndexPath) -> UITableViewCell {
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "infoValueCell") as! IFMInfoValueCell
		
		cell.bindViewModel(self.viewModel.informationCellModels[(indexPath as NSIndexPath).row])
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if (indexPath as NSIndexPath).section == 3 {
			return 80
		}
		return 40
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0.000000001
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if section == 0 || section == 1{
			return 30
		}
		else if section == 3 {
			return 0.00000001
		}
		return 50
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let view = Bundle.main.loadNibNamed("IFMHeaderInputView", owner: self, options: nil)?.first as! IFMHeaderInputView
		
		if section == 0  || section == 1 {
			view.titleLabel.text = ""
		}
		else if section == 2{
			view.titleLabel.text = "Information"
		}
		else {
			return UIView()
		}
		return view
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if (indexPath as NSIndexPath).section == 0 {
			if (indexPath as NSIndexPath).row == 0 {
				self.navigateToMonthOptions()
			}
			else if (indexPath as NSIndexPath).row == 1 {
				self.navigateToPeriodeOptions()
			}
			else if (indexPath as NSIndexPath).row == 2 {
				self.navigateToProductOptions()
			}
			else if (indexPath as NSIndexPath).row == 3 {
				self.navigateToTbbmOptions()
			}
			else if (indexPath as NSIndexPath).row == 4 {
				self.navigateToPbbkbOptions()
			}
			else if (indexPath as NSIndexPath).row == 5 {
				self.showVolumeInput()
			}
		}
		else if (indexPath as NSIndexPath).section == 1 {
			if (indexPath as NSIndexPath).row == 0 {
				self.navigateToMarginBasedOnOptions()
			}
			else {
				self.showPriceInput()
			}
		}
	}
	
	func navigateToOutput() {
		let vc = IFMMarginOutputViewController(viewModel : self.viewModel.marginOutputViewModel())
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	func navigateToMonthOptions() {
		let vm = self.viewModel.monthOptionsViewModel()
		let vc = IFMMenuOptionsViewController(viewModel : vm)
		vc.selectedSignal().subscribeNext { (obj : Any!) in
			let index = obj as! Int
			self.viewModel.selectMonth(index)
			self.viewModel.getPublishAndMOPS()
			self.tableView.reloadData()
		}
		
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	func navigateToPbbkbOptions() {
		let vm = self.viewModel.pbbkbOptionsViewModel()
		let vc = IFMMenuOptionsViewController(viewModel : vm)
		vc.selectedSignal().subscribeNext { (obj : Any!) in
			let index = obj as! Int
			self.viewModel.selectPbbkb(index)
			self.tableView.reloadData()
		}
		
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	func navigateToTbbmOptions() {
		let vm = self.viewModel.ttbmOptionsViewModel()
		let vc = IFMMenuOptionsViewController(viewModel : vm)
		vc.selectedSignal().subscribeNext { (obj : Any!) in
			let index = obj as! Int
			self.viewModel.selectTbbm(index)
			self.tableView.reloadData()
		}
		
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	func navigateToProductOptions() {
		let vm = self.viewModel.productsOptionsViewModel()
		let vc = IFMMenuOptionsViewController(viewModel : vm)
		vc.selectedSignal().subscribeNext { (obj : Any!) in
			let index = obj as! Int
			self.viewModel.selectProduct(index)
			self.viewModel.getPublishAndMOPS()
			self.tableView.reloadData()
		}
		
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	func navigateToPeriodeOptions() {
		let vm = self.viewModel.periodeOptionsViewModel()
		let vc = IFMMenuOptionsViewController(viewModel : vm)
		vc.selectedSignal().subscribeNext { (obj : Any!) in
			let index = obj as! Int
			self.viewModel.selectPeriode(index)
			self.viewModel.getPublishAndMOPS()
			self.tableView.reloadData()
		}
		
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	func discountEntered(_ alert: UIAlertAction!){
		self.viewModel.inputMargin(self.marginField.text!)
		self.tableView.reloadData()
	}
	
	func addDiscountField(_ textField: UITextField!){
		textField.keyboardType = .decimalPad
		self.marginField = textField
	}
	
	
	func showPriceInput() {
		let pricePrompt = UIAlertController(title: "", message: "Input the Margin", preferredStyle: UIAlertControllerStyle.alert)
		pricePrompt.addTextField(configurationHandler: self.addDiscountField)
		pricePrompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
		pricePrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: discountEntered))
		present(pricePrompt, animated: true, completion: nil)
	}
	
	
	func volumeEntered(_ alert: UIAlertAction!){
		self.viewModel.inputVolume(self.volumeField.text!)
		self.tableView.reloadData()
	}
	
	func addVolumeTextField(_ textField: UITextField!){
		textField.keyboardType = .decimalPad
		self.volumeField = textField
	}
	
	
	func showVolumeInput() {
		let pricePrompt = UIAlertController(title: "", message: "Input the Volume", preferredStyle: UIAlertControllerStyle.alert)
		pricePrompt.addTextField(configurationHandler: self.addVolumeTextField)
		pricePrompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
		pricePrompt.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: volumeEntered))
		present(pricePrompt, animated: true, completion: nil)
	}
	
	func navigateToMarginBasedOnOptions() {
		let vm = self.viewModel.marginBasedOnViewModel()
		let vc = IFMMenuOptionsViewController(viewModel : vm)
		vc.selectedSignal().subscribeNext { (obj : Any!) in
			let index = obj as! Int
			self.viewModel.selectMarginBasedOn(index)
			self.tableView.reloadData()
		}
		
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	func formatNumber(_ number : Double) -> String {
		let formatter = NumberFormatter()
		formatter.locale = Locale.current
		formatter.numberStyle = NumberFormatter.Style.decimal
		formatter.maximumFractionDigits = 2
		
		return formatter.string(from: NSNumber(value: number as Double))!
	}


}
