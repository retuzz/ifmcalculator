//
//  IFMMenuOptionsViewModel.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMMenuOptionsViewModel: NSObject {
	var cellModels : [IFMOptionCellModel]!
	
	func selectRow(_ row : Int) {
		self.resetAll()
		self.cellModels[row].selected = true
	}
	
	func resetAll() {
		for cellModel in self.cellModels {
			cellModel.selected = false
		}
	}
}
