//
//  IFMPriceViewModel.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/21/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMPriceViewModel: NSObject {

	let apiService : IFMAPIService = IFMAPIService()
	
	var formCellModels : [IFMInputCellModel]!
	
	var monthCellModel : IFMInputCellModel = IFMInputCellModel()
	var periodCellModel : IFMInputCellModel = IFMInputCellModel()
	var productCellModel : IFMInputCellModel = IFMInputCellModel()
	var tbbmCellModel :  IFMInputCellModel = IFMInputCellModel()
	var pbbkbCellModel : IFMInputCellModel = IFMInputCellModel()
	var volumeCellModel : IFMInputCellModel = IFMInputCellModel()
	
	var offeringCellModel : IFMInfoValueCellModel = IFMInfoValueCellModel()
	
	var informationCellModels : [IFMInfoValueCellModel]!
	var publishPriceCellModel : IFMInfoValueCellModel = IFMInfoValueCellModel()
	var mopsCellModel : IFMInfoValueCellModel = IFMInfoValueCellModel()
	
	var selectedMonthIndex : Int?
	let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
	
	var selectedPbbkbIndex : Int?
	let pbbkbText = ["7.5%", "6.75%", "1.5%","1.288%", "0%"]
	let pbbkbAmount = [7.5, 6.75, 1.5, 1.288, 0]
	
	var selectedProductIndex : Int?
	var priceAmount : Double?
	var volumeAmount : Double?
	
	var products : [IFMProduct]?
	var tbbms : [IFMTbbm]?
	var periods : [IFMPeriode]?
	
	var selectedPeriodeIndex : Int?
	var mopsAmount : Double?
	var publishAmount : Double?
	var hppAmount : Double?
	
	var selectedTbbmIndex : Int?
	
	var refreshSubject : RACSubject = RACSubject()
	
	override init() {
		super.init()
		let defaults = UserDefaults.standard
		self.apiService.setupToken(defaults.object(forKey: "token") as! String)
		
		self.setupFirstSection()
	}
	
	func setupFirstSection() {
		monthCellModel.title = "Month"
		periodCellModel.title = "Period"
		productCellModel.title = "Product"
		tbbmCellModel.title = "TBBM"
		pbbkbCellModel.title = "PBBKB"
		volumeCellModel.title = "Volume (KL)"
		
		formCellModels = [monthCellModel, periodCellModel, productCellModel,tbbmCellModel, pbbkbCellModel, volumeCellModel]
		
		offeringCellModel.title = "Price Rp/KL"
		
		mopsCellModel.title = "MOPS (Rp)"
		publishPriceCellModel.title = "Publish Price (Rp)"
	
		informationCellModels = [publishPriceCellModel, mopsCellModel]
	}
	
	func refreshSignal() -> RACSignal {
		return self.refreshSubject
	}
	
	func monthOptionsViewModel() -> IFMMenuOptionsViewModel {
		let vm = IFMMenuOptionsViewModel()
		var cellModels = [IFMOptionCellModel]()
		
		for month in self.months {
			let cellModel = IFMOptionCellModel()
			cellModel.value = month
			
			cellModels.append(cellModel)
		}
		
		if self.selectedMonthIndex != nil {
			cellModels[self.selectedMonthIndex!].selected = true
		}
		
		vm.cellModels = cellModels
		
		return vm
	}
	
	func selectMonth(_ index : Int) {
		self.selectedMonthIndex = index
		self.monthCellModel.value = self.months[index]
	}
	
	func pbbkbOptionsViewModel() -> IFMMenuOptionsViewModel {
		let vm = IFMMenuOptionsViewModel()
		var cellModels = [IFMOptionCellModel]()
		
		for pbbkb in self.pbbkbText {
			let cellModel = IFMOptionCellModel()
			cellModel.value = pbbkb
			
			cellModels.append(cellModel)
		}
		
		if self.selectedPbbkbIndex != nil {
			cellModels[self.selectedPbbkbIndex!].selected = true
		}
		
		vm.cellModels = cellModels
		
		return vm
	}
	
	func selectPbbkb(_ index : Int) {
		self.selectedPbbkbIndex = index
		self.pbbkbCellModel.value = self.pbbkbText[index]
	}
	
	func inputPrice(_ price : String) {
		if let number = NumberFormatter().number(from: price) {
			let float = Double(number)
			self.priceAmount = float
			self.offeringCellModel.value = self.formatNumber(Double(price)!)
		}
		
	}
	
	func inputVolume(_ volume : String) {
		if let number = NumberFormatter().number(from: volume) {
			let float = Double(number)
			self.volumeAmount = float
			self.volumeCellModel.value = volume
		}
		
	}
	
	func productsOptionsViewModel() -> IFMMenuOptionsViewModel {
		let vm = IFMMenuOptionsViewModel()
		var cellModels = [IFMOptionCellModel]()
		self.products = TMCache.shared().object(forKey: "ifmproducts") as? [IFMProduct]
		
		if self.products != nil {
			for product in self.products! {
				let cellModel = IFMOptionCellModel()
				cellModel.value = product.name
				
				cellModels.append(cellModel)
			}
		}
		if self.selectedProductIndex != nil {
			cellModels[self.selectedProductIndex!].selected = true
		}
		
		vm.cellModels = cellModels
		
		return vm
	}
	
	func periodeOptionsViewModel() -> IFMMenuOptionsViewModel {
		let vm = IFMMenuOptionsViewModel()
		var cellModels = [IFMOptionCellModel]()
		self.periods = TMCache.shared().object(forKey: "ifmperiods") as? [IFMPeriode]
		
		if self.periods != nil {
			for period in self.periods! {
				let cellModel = IFMOptionCellModel()
				cellModel.value = period.name.stringValue
				
				cellModels.append(cellModel)
			}
		}
		if self.selectedPeriodeIndex != nil {
			cellModels[self.selectedPeriodeIndex!].selected = true
		}
		
		vm.cellModels = cellModels
		
		return vm
	}
	
	func ttbmOptionsViewModel() -> IFMMenuOptionsViewModel {
		let vm = IFMMenuOptionsViewModel()
		var cellModels = [IFMOptionCellModel]()
		self.tbbms = TMCache.shared().object(forKey: "ifmtbbm") as? [IFMTbbm]
		
		if self.tbbms != nil {
			for period in self.tbbms! {
				let cellModel = IFMOptionCellModel()
				cellModel.value = period.name
				
				cellModels.append(cellModel)
			}
		}
		if self.selectedTbbmIndex != nil {
			cellModels[self.selectedTbbmIndex!].selected = true
		}
		
		vm.cellModels = cellModels
		
		return vm
	}
	
	func selectTbbm(_ index : Int) {
		self.selectedTbbmIndex = index
		self.tbbmCellModel.value = self.tbbms![index].name
	}
	
	func selectProduct(_ index : Int) {
		self.selectedProductIndex = index
		self.productCellModel.value = self.products![index].name
	}
	
	func selectPeriode(_ index : Int) {
		self.selectedPeriodeIndex = index
		self.periodCellModel.value = self.periods![index].name.stringValue
	}
	
	func getPublishAndMOPS() {
		
		if self.selectedPeriodeIndex != nil && self.selectedProductIndex != nil && self.selectedMonthIndex != nil {
			let productID = self.products![self.selectedProductIndex!].productID
			let periodeID = self.periods![self.selectedPeriodeIndex!].periodeID
			let monthID = self.selectedMonthIndex! + 1
			
			self.apiService.fetchHargaPublishMOPS(productID!, monthID: NSNumber(value: monthID as Int), periodeID: periodeID!).subscribeNext({ (obj : Any!) in
				
				let tuple = obj as! RACTuple
				let dict = tuple.second as! [String : AnyObject]
				
				let publish = dict["published_price"] as! String
				let mops = dict["mops"] as! String
				
				self.mopsCellModel.value = self.formatNumber(Double(mops)!)
				self.publishPriceCellModel.value = self.formatNumber(Double(publish)!)
				
				self.publishAmount = Double(publish)
				self.mopsAmount = Double(mops)
				
				self.refreshSubject.sendNext(nil)
			})
		}
	}
	
	func priceOutputViewModel() -> IFMPriceOutputViewModel {
		if self.publishAmount != nil && self.mopsAmount != nil && self.selectedPbbkbIndex != nil && self.priceAmount != nil && self.volumeAmount != nil {
			let vm = IFMPriceOutputViewModel(publishedPrice: self.publishAmount!, mops: self.mopsAmount!, customerPrice: self.priceAmount!, pbbKb: self.pbbkbAmount[self.selectedPbbkbIndex!], volume: self.volumeAmount!, hpp: self.hppAmount!)
			return vm
		}
		
		return IFMPriceOutputViewModel()
	}
	
	func getHpp() -> RACSignal {
		if self.selectedProductIndex != nil && self.selectedPeriodeIndex != nil && self.selectedMonthIndex != nil {
			let productID = self.products![self.selectedProductIndex!].productID
			let periodeID = self.periods![self.selectedPeriodeIndex!].periodeID
			let monthID = self.selectedMonthIndex! + 1

			if productID != nil && periodeID != nil && monthID > 0 && self.selectedTbbmIndex != nil {
				return self.apiService.fetchHpp(productID!, monthID: NSNumber(value:monthID), periodeID: periodeID!, tbbmID: self.tbbms![self.selectedTbbmIndex!].tbbmID).map({ (obj : Any!) -> AnyObject! in
					let tuple = obj as! RACTuple
					let dict = tuple.second as! [String : AnyObject]
					
					let percent = dict["percent"] as! String
					self.hppAmount = Double(percent)
					
					return obj as AnyObject!
				})
			}
			
		}
		
		return RACSignal.error(nil)
	}

	func formatNumber(_ number : Double) -> String {
		let formatter = NumberFormatter()
		formatter.locale = Locale.current
		formatter.numberStyle = NumberFormatter.Style.decimal
		formatter.maximumFractionDigits = 2
		
		return formatter.string(from: NSNumber(value: number as Double))!
	}
}
