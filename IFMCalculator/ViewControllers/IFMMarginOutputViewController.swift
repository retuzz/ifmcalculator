//
//  IFMMarginOutputViewController.swift
//  IFMCalculator
//
//  Created by Margareta Hardiyanti on 8/22/16.
//  Copyright © 2016 Pertamina. All rights reserved.
//

import UIKit

class IFMMarginOutputViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{

	@IBOutlet weak var segmentedControl: UISegmentedControl!
	@IBOutlet weak var tableView: UITableView!
	var viewModel : IFMMarginOutputViewModel!
	
	init(viewModel : IFMMarginOutputViewModel) {
		super.init(nibName: "IFMMarginOutputViewController", bundle: nil)
		self.viewModel = viewModel
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setDefaultNavigation()
		self.setNavigationBackBar()
		
		self.title = "Margin Result"
		self.setupViewModel()
		self.setupSegmentation()
		self.setupTableView()
	}
	
	func setNavigationBackBar() {
		self.navigationItem.hidesBackButton = true
		let menuButtonImg = UIImage(named: "back")
		let leftMenuButton = UIBarButtonItem(image: menuButtonImg?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), style: UIBarButtonItemStyle.plain, target: self, action: #selector(navigateBack))
		leftMenuButton.tintColor = IFMColorObject.sharedInstance.tosca
		
		
		self.navigationItem.leftBarButtonItem = leftMenuButton
	}
	
	func navigateBack() {
		_ = self.navigationController?.popViewController(animated: true)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func setDefaultNavigation(){
		self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName : IFMFontObject.sharedInstance.bodyCopy1!, NSForegroundColorAttributeName: IFMColorObject.sharedInstance.tosca]
		self.navigationController?.navigationBar.isHidden = false
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.shadowImage = nil
	}
	
	func setupTableView() {
		self.tableView.register(UINib(nibName: "IFMInfoValueCell", bundle: nil), forCellReuseIdentifier: "infoValueCell")
		
		self.tableView.dataSource = self
		self.tableView.delegate = self
		self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
		self.tableView.tableFooterView = UIView()
	}
	
	func setupSegmentation() {
		self.segmentedControl.rac_signal(for: UIControlEvents.valueChanged).subscribeNext { (obj : Any!) in
			self.tableView.reloadData()
		}
	}
	func setupViewModel() {
	}
	
	//MARK : TableView
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 2
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if self.segmentedControl.selectedSegmentIndex == 0 {
			return numberOfRowsOnCalculation(section)
		}
		else {
			return numberOfRowsOnBilling(section)
		}
	}
	
	func numberOfRowsOnCalculation(_ section : Int) -> Int {
		if section == 0 {
			return self.viewModel.firstSectionCalculationCellModels.count
		}
		return 1
	}
	
	func numberOfRowsOnBilling(_ section : Int) -> Int{
		if section == 0 {
			return self.viewModel.firstSectionBillingCellModels.count
		}
		return self.viewModel.secondSectionBillingCellModels.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 40
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if self.segmentedControl.selectedSegmentIndex == 0 {
			return cellOnCalculation(indexPath)
		}
		else {
			return cellOnBilling(indexPath)
		}
	}
	
	func cellOnCalculation(_ indexPath : IndexPath) -> UITableViewCell {
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "infoValueCell") as! IFMInfoValueCell
		if (indexPath as NSIndexPath).section == 0 {
			cell.bindViewModel(self.viewModel.firstSectionCalculationCellModels[(indexPath as NSIndexPath).row])
		}
		else {
			cell.bindViewModel(self.viewModel.profitCellModel)
		}
		
		return cell
	}
	
	func cellOnBilling(_ indexPath : IndexPath) -> UITableViewCell {
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "infoValueCell") as! IFMInfoValueCell
		
		if (indexPath as NSIndexPath).section == 0 {
			cell.bindViewModel(self.viewModel.firstSectionBillingCellModels[(indexPath as NSIndexPath).row])
		}
		else {
			cell.bindViewModel(self.viewModel.secondSectionBillingCellModels[(indexPath as NSIndexPath).row])
		}
		return cell
		
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if self.segmentedControl.selectedSegmentIndex == 0 {
			return headerHeightOnCalculation(section)
		}
		else {
			return headerHeightOnBilling(section)
		}
	}
	
	func headerHeightOnCalculation(_ section : Int) -> CGFloat {
		if section == 0 {
			return 0.000000001
		}
		return 50
	}
	
	func headerHeightOnBilling(_ section : Int) -> CGFloat {
		return 50
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if self.segmentedControl.selectedSegmentIndex == 0 {
			return viewHeaderOnCalculation(section)
		}
		else {
			return viewHeaderOnBilling(section)
		}
	}
	
	func viewHeaderOnCalculation(_ section : Int) -> UIView {
		let view = Bundle.main.loadNibNamed("IFMHeaderInputView", owner: self, options: nil)?.first as! IFMHeaderInputView
		
		if section == 0 {
			return UIView()
		}
		else {
			view.titleLabel.text = "Profit"
		}
		
		return view
	}
	
	func viewHeaderOnBilling(_ section : Int) -> UIView {
		let view = Bundle.main.loadNibNamed("IFMHeaderInputView", owner: self, options: nil)?.first as! IFMHeaderInputView
		
		if section == 0 {
			view.titleLabel.text = "Billing 1 KL"
		}
		else {
			view.titleLabel.text = "Billing Total"
		}
		
		return view
	}
	

}
